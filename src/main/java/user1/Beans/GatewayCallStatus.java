package user1.Beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import user1.Enums.GatewayCallState;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GatewayCallStatus {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("protocol")
    private String protocol;

    @JsonProperty("state")
    private GatewayCallState state;

    @JsonProperty("name")
    private String name;

    @JsonProperty("attendee_id")
    private String attendeeId;

    @JsonProperty("audio_only")
    private Boolean audioOnly;

    @JsonProperty("duration_ms")
    private Long duration;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public GatewayCallState getState() {
        return state;
    }

    public void setState(GatewayCallState state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttendeeId() {
        return attendeeId;
    }

    public void setAttendeeId(String attendeeId) {
        this.attendeeId = attendeeId;
    }

    public Boolean getAudioOnly() {
        return audioOnly;
    }

    public void setAudioOnly(Boolean audioOnly) {
        this.audioOnly = audioOnly;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }
}

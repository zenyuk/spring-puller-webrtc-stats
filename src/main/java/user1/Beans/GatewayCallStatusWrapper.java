package user1.Beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GatewayCallStatusWrapper {
    private Map<String, GatewayCallStatus> calls;

    public Map<String, GatewayCallStatus> getCalls() {
        return calls;
    }

    public void setCalls(Map<String, GatewayCallStatus> calls) {
        this.calls = calls;
    }
}
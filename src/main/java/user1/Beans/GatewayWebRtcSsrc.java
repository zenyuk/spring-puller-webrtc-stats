package user1.Beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import user1.Enums.GatewayCallState;
import user1.Enums.MediaType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GatewayWebRtcSsrc {
    private Long ssrc;
    private Long packetsLost;
    private Long packetsSent;
    private Long packetsReceived;
    private MediaType mediaType;
    private String googCodecName;
    private String timestamp;

    public Long getSsrc() {
        return ssrc;
    }

    public void setSsrc(Long ssrc) {
        this.ssrc = ssrc;
    }

    public Long getPacketsLost() {
        return packetsLost;
    }

    public void setPacketsLost(Long packetsLost) {
        this.packetsLost = packetsLost;
    }

    public Long getPacketsSent() {
        return packetsSent;
    }

    public void setPacketsSent(Long packetsSent) {
        this.packetsSent = packetsSent;
    }

    public Long getPacketsReceived() {
        return packetsReceived;
    }

    public void setPacketsReceived(Long packetsReceived) {
        this.packetsReceived = packetsReceived;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public String getGoogCodecName() {
        return googCodecName;
    }

    public void setGoogCodecName(String googCodecName) {
        this.googCodecName = googCodecName;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}

package user1.Beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GatewayCallWebRtcStatus {
    private String callId;
    private Long audioRecvPacketsLost;
    private Long audioSendPacketsLost;
    private Long videoRecvPacketsLost;
    private Long videoSendPacketsLost;
    private Long audioRecvPacketsTotal;
    private Long audioSendPacketsTotal;
    private Long videoRecvPacketsTotal;
    private Long videoSendPacketsTotal;
    private String audioCodecName;
    private String videoCodecName;
    private String timestamp;

    public String getCallId() {
        return callId;
    }

    public void setCallId(String callId) {
        this.callId = callId;
    }

    public Long getAudioRecvPacketsLost() {
        return audioRecvPacketsLost;
    }

    public void setAudioRecvPacketsLost(Long audioRecvPacketsLost) {
        this.audioRecvPacketsLost = audioRecvPacketsLost;
    }

    public Long getAudioSendPacketsLost() {
        return audioSendPacketsLost;
    }

    public void setAudioSendPacketsLost(Long audioSendPacketsLost) {
        this.audioSendPacketsLost = audioSendPacketsLost;
    }

    public Long getVideoRecvPacketsLost() {
        return videoRecvPacketsLost;
    }

    public void setVideoRecvPacketsLost(Long videoRecvPacketsLost) {
        this.videoRecvPacketsLost = videoRecvPacketsLost;
    }

    public Long getVideoSendPacketsLost() {
        return videoSendPacketsLost;
    }

    public void setVideoSendPacketsLost(Long videoSendPacketsLost) {
        this.videoSendPacketsLost = videoSendPacketsLost;
    }

    public Long getAudioRecvPacketsTotal() {
        return audioRecvPacketsTotal;
    }

    public void setAudioRecvPacketsTotal(Long audioRecvPacketsTotal) {
        this.audioRecvPacketsTotal = audioRecvPacketsTotal;
    }

    public Long getAudioSendPacketsTotal() {
        return audioSendPacketsTotal;
    }

    public void setAudioSendPacketsTotal(Long audioSendPacketsTotal) {
        this.audioSendPacketsTotal = audioSendPacketsTotal;
    }

    public Long getVideoRecvPacketsTotal() {
        return videoRecvPacketsTotal;
    }

    public void setVideoRecvPacketsTotal(Long videoRecvPacketsTotal) {
        this.videoRecvPacketsTotal = videoRecvPacketsTotal;
    }

    public Long getVideoSendPacketsTotal() {
        return videoSendPacketsTotal;
    }

    public void setVideoSendPacketsTotal(Long videoSendPacketsTotal) {
        this.videoSendPacketsTotal = videoSendPacketsTotal;
    }

    public String getAudioCodecName() {
        return audioCodecName;
    }

    public void setAudioCodecName(String audioCodecName) {
        this.audioCodecName = audioCodecName;
    }

    public String getVideoCodecName() {
        return videoCodecName;
    }

    public void setVideoCodecName(String videoCodecName) {
        this.videoCodecName = videoCodecName;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
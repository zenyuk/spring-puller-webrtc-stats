package user1.Beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GatewayWebRtcSsrcWrapper {
    private Map<String, GatewayWebRtcSsrc> stats;

    public Map<String, GatewayWebRtcSsrc> getStats() {
        return stats;
    }

    public void setStats(Map<String, GatewayWebRtcSsrc> stats) {
        this.stats = stats;
    }
}
package user1.Services;

import org.springframework.web.client.RestTemplate;
import java.util.HashMap;
import java.util.Map;

import user1.Beans.GatewayCallStatus;
import user1.Beans.GatewayCallStatusWrapper;
import user1.Beans.GatewayCallWebRtcStatus;
import user1.Enums.GatewayCallState;
import user1.Parsers.WebRtcStatsParser;


public class GatewayWebRtcStatusService {
    static String baseApiUri = "http://dev-syd-edge.faceme.com";
    static String webrtcStatsApiPath = "/webrtc_stats";

    public Map<String, GatewayCallWebRtcStatus> getGatewayInstanceWebRtcCallsStatus(int instancePort) {
        Map<String, GatewayCallWebRtcStatus> result = new HashMap<String, GatewayCallWebRtcStatus>();

        Map<String, GatewayCallStatus> calls =  getActiveCallsStatsApiCall(instancePort);
        if (calls == null)
            return result;

        for(Map.Entry<String, GatewayCallStatus> item: calls.entrySet()) {
            String callGuid = item.getKey();
            GatewayCallStatus callStatus = item.getValue();
            if (callStatus.getState() != GatewayCallState.CONNECTED) {
                continue;
            }

            GatewayCallWebRtcStatus webRtcStatus = getWebRtcCallStatusApiCall(instancePort, callGuid, callStatus.getId());
            if (webRtcStatus != null) {
                result.put(callGuid, webRtcStatus);
            }
        }
        return result;
    }

    private Map<String, GatewayCallStatus> getActiveCallsStatsApiCall(int port) {
        RestTemplate restTemplate = new RestTemplate();
        GatewayCallStatusWrapper response = null;
        try {
            response = restTemplate.getForObject(baseApiUri + ":" + port, GatewayCallStatusWrapper.class);
        } catch (Exception e) {
            System.out.println("Error getting response for WebRTC stats call for URI: " + baseApiUri + ":" + port);
        }

        return response.getCalls();
    }

    private GatewayCallWebRtcStatus getWebRtcCallStatusApiCall(int port, String callGuid, Long callId) {
        if (callId == null)
            return null;

        RestTemplate restTemplate = new RestTemplate();
        String url = baseApiUri + ":" + port + "/calls/" + callId + webrtcStatsApiPath;
        String r = restTemplate.getForObject(url, String.class);
        return new WebRtcStatsParser()
                .parseGatewayWebRtcStatus(callGuid, r);
    }
}

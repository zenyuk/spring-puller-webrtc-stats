package user1.Enums;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public enum MediaType {
    @JsonProperty("audio")
    AUDIO,

    @JsonProperty("video")
    VIDEO
}

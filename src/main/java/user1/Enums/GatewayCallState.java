package user1.Enums;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public enum GatewayCallState {
    @JsonProperty("CONNECTED")
    CONNECTED,

    @JsonProperty("DISCONNECTED")
    DISCONNECTED,

    @JsonProperty("FAILED")
    FAILED
}

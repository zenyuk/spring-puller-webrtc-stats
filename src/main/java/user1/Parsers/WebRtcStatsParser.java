package user1.Parsers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import user1.Beans.GatewayCallWebRtcStatus;
import user1.Beans.GatewayWebRtcSsrc;
import user1.Beans.GatewayWebRtcSsrcWrapper;
import user1.Enums.MediaType;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

public class WebRtcStatsParser {
    public GatewayCallWebRtcStatus parseGatewayWebRtcStatus(String callGuid, String input) {
        String separator = "\"ssrc_";
        // cut useless beginning and wrap for parsing
        input = "{\"stats\":{" + separator + StringUtils.substringAfter(input, separator) + "}";

        GatewayWebRtcSsrcWrapper statsWrapped;
        ObjectMapper mapper = new ObjectMapper();
        try {
            statsWrapped = mapper.readValue(input, GatewayWebRtcSsrcWrapper.class);
        } catch (IOException e) {
            System.out.println("Error parsing SSRC WebRTC stats for a Call with GUID: " + callGuid);
            return null;
        }

        Map<String, GatewayWebRtcSsrc> stats = statsWrapped.getStats();

        GatewayCallWebRtcStatus result = new GatewayCallWebRtcStatus();
        result.setCallId(callGuid);

        for (Iterator<Map.Entry<String, GatewayWebRtcSsrc>> i = stats.entrySet().iterator(); i.hasNext();) {
            Map.Entry<String, GatewayWebRtcSsrc> entry = i.next();
            GatewayWebRtcSsrc ssrc = entry.getValue();
            if (ssrc.getSsrc() == null) {
                continue;
            }
            setSsrcStatsOneWay(ssrc, entry.getKey().contains("send"), result);
        }
        return result;
    }

    private void setSsrcStatsOneWay(GatewayWebRtcSsrc ssrc, boolean isSendDirection, GatewayCallWebRtcStatus result) {
        if (isSendDirection) {
            if (ssrc.getMediaType() == MediaType.AUDIO) {
                result.setAudioSendPacketsLost(ssrc.getPacketsLost());
                result.setAudioSendPacketsTotal(ssrc.getPacketsSent());
                result.setAudioCodecName(ssrc.getGoogCodecName());
            } else if (ssrc.getMediaType() == MediaType.VIDEO) {
                result.setVideoSendPacketsLost(ssrc.getPacketsLost());
                result.setVideoSendPacketsTotal(ssrc.getPacketsSent());
                result.setVideoCodecName(ssrc.getGoogCodecName());
            }
        } else {
            if (ssrc.getMediaType() == MediaType.AUDIO) {
                result.setAudioRecvPacketsLost(ssrc.getPacketsLost());
                result.setAudioRecvPacketsTotal(ssrc.getPacketsReceived());
            } else if (ssrc.getMediaType() == MediaType.VIDEO) {
                result.setVideoRecvPacketsLost(ssrc.getPacketsLost());
                result.setVideoRecvPacketsTotal(ssrc.getPacketsReceived());
                result.setTimestamp(ssrc.getTimestamp());
            }
        }
    }
}
